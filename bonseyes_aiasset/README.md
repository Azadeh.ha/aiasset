# Bonseyes AI Asset | aiasset

Bonseyes AI Asset represents an implementation of the research paper employing the deep learning deployment-centric 
framework [AI Asset Container](https://gitlab.com/bonseyes/assets/aiasset_container_generator). 
AI Asset Container provides a set of services and standardized building components that facilitates and accelerates 
the development of AI systems for the resource constrained low power devices.

**Documentation:** <a href="" target="_blank"> https://bonseyes.gitlab.io/assets/<repository_name> </a>
