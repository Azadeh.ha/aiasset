import os
import subprocess

from bonseyes_aiasset.utils.meters import HardwareStatus


class TestInterfaceGpu:
    def test_interface_export_onnx(self):
        # todo change backbone option
        rc = subprocess.call(
            "python /app/interface/exporter.py --input-sizes 120x120 --engine onnxruntime --precisions fp32 --backbone <backbone>",
            shell=True)

        onnx_models = os.listdir(f'/app/bonseyes_aiasset/models/onnx/<backbone>')
        assert len(onnx_models) == 1
        assert rc == 0

    def test_interface_export(self):
        # todo change backbone option
        rc = subprocess.call(
            "python /app/interface/exporter.py --input-sizes 120x120 --engine all --backbone <backbone> --precisions fp16 fp32",
            shell=True)

        hws = HardwareStatus()
        gpu_name = hws.get()['gpu_name']
        tensorrt_models = os.listdir(f'/app/bonseyes_aiasset/models/tensorrt/{gpu_name}/<backbone>')

        assert len(tensorrt_models) == 2
        assert rc == 0

    def test_interface_optimize(self):
        # todo change backbone option
        rc = subprocess.call(
            "python /app/interface/optimizer.py --input-sizes 120x120 --engine all --backbone <backbone>",
            shell=True)

        hws = HardwareStatus()
        gpu_name = hws.get()['gpu_name']
        tensorrt_models = os.listdir(f'/app/bonseyes_aiasset/models/tensorrt/{gpu_name}/<backbone>')
        onnx_models = os.listdir(f'/app/bonseyes_aiasset/models/onnx/<backbone>')

        assert len(tensorrt_models) == 3
        assert len(onnx_models) == 2
        assert rc == 0

    def test_interface_image_onnx(self):
        # todo change backbone option
        # todo change jpg-input option
        rc = subprocess.call(
            """python /app/interface/image_processor.py \
                --input-size 120x120 \
                --engine onnxruntime \
                --backbone <backbone> \
                --device gpu \
                --jpg-input interface/tests/samples/image/<demo_image>""",
            shell=True)
        assert rc == 0

    def test_interface_image_pytorch(self):
        # todo change backbone option
        # todo change jpg-input option
        rc = subprocess.call(
            """python /app/interface/image_processor.py \
                --input-size 120x120 \
                --engine pytorch \
                --backbone <backbone> \
                --device gpu \
                --jpg-input interface/tests/samples/image/<demo_image>""",
            shell=True)
        assert rc == 0

    def test_interface_image_tensorrt(self):
        # todo change backbone option
        # todo change jpg-input option
        rc = subprocess.call(
            """python /app/interface/image_processor.py \
                --input-size 120x120 \
                --engine tensorrt \
                --backbone <backbone> \
                --device gpu \
                --jpg-input interface/tests/samples/image/<demo_image>""",
            shell=True)
        assert rc == 0

    def test_interface_video_onnx(self):
        # todo change backbone option
        # todo change video-input option
        rc = subprocess.call(
            """python /app/interface/video_processor.py \
                --input-size 120x120 \
                --engine onnxruntime \
                --backbone <backbone> \
                --device gpu \
                --video-input interface/tests/samples/video/<demo_video>""",
            shell=True)
        assert rc == 0

    def test_interface_video_pytorch(self):
        # todo change backbone option
        # todo change video-input option
        rc = subprocess.call(
            """python /app/interface/video_processor.py \
                --input-size 120x120 \
                --engine pytorch \
                --backbone <backbone> \
                --device gpu \
                --video-input interface/tests/samples/video/<demo_video>""",
            shell=True)
        assert rc == 0

    def test_interface_video_tensorrt(self):
        # todo change backbone option
        # todo change video-input option
        rc = subprocess.call(
            """python /app/interface/video_processor.py \
                --input-size 120x120 \
                --engine tensorrt \
                --backbone <backbone> \
                --device gpu \
                --video-input interface/tests/samples/video/<demo_video>""",
            shell=True)
        assert rc == 0

    def test_interface_benchmark(self):
        # todo change backbone option
        # todo change dataset option
        rc = subprocess.call(
            """python /app/interface/benchmark.py \
                --input-sizes 120x120 \
                --dataset <dataset> \
                --engine all \
                --backbone <backbone> \
                --device gpu""",
            shell=True)
        assert rc == 0
