import subprocess

from setuptools import setup, find_packages


def post_install():
    """ Implement post installation routine """


def pre_install():
    """ Implement pre installation routine """


pre_install()

setup(
    name='bonseyes_aiasset',
    version='0.0.1',
    url='https://gitlab.com/Azadeh.ha/aiasset.git',
    description='AI asset',

    author='azadeh',
    author_email='azadeh.haratian@gmail.com',
    zip_safe=False,

    packages=find_packages(),
    install_requires=[
        # AI Asset container dependencies
        'PyTurboJPEG == 1.5.0',
        'requests == 2.25.1',
        'PyYAML == 5.4.1',
        'Flask == 1.1.2',
        'tqdm == 4.60.0',
    ],
    extras_require={
        'x86': [
            # x86 dependencies
            # TODO add missing
            'pre-commit == 2.13.0',
            'nvidia-ml-py3 == 7.352.0',
        ],
        'nvidia_jetson': [
            # Nvidia Jetson dependencies
            # TODO add missing
            'onnx==1.8.0',
            'gdown==3.13.0'
        ],
        'rpi_arm64v8': [
            # RPI 4 dependencies
            # TODO add missing
            'onnx==1.8.0',
            'gdown==3.13.0'
        ],
    },
)

post_install()
