import argparse
import torch
import os
import csv
import re

from torchstat import stat
from bonseyes_aiasset.algorithm.algorithm import Algorithm


def cli():
    parser = argparse.ArgumentParser(description='Bonseyes AI Asset model summary.')
    parser.add_argument('--model', '-mo', required=True, type=str, help='Path to model file')
    parser.add_argument(
        '--input-size',
        '-is',
        required=False,
        default='<default_size>',  # TODO
        type=str,
        help='Model input size in format: WIDTHxHEIGHT',
    )
    return parser


def main():
    args = cli().parse_args()

    # TODO
    # Initialize algorithm
    algorithm = Algorithm(
        model_path=args.model,
        engine_type='pytorch',
        input_size=args.input_size,
        device=args.device,
        cpu_num=args.cpu_num,
        thread_num=args.thread_num,
        # + additional arguments
    )

    model = algorithm.model
    stat(model, (3, args.input_size[1], args.input_size[0]))


if __name__ == '__main__':
    main()