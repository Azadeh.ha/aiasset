import argparse
import logging
import sys
import os

sys.path.append("/app/source/Submodule name")

import onnx.utils
import onnxsim
import onnx
import torch
from typing import List
import models

LOG = logging.getLogger(__name__)


def image_size_warning(basenet_stride, input_w, input_h):
    if input_w % basenet_stride != 1:
        LOG.warning(
            'input width (%d) should be a multiple of basenet ' 'stride (%d) + 1: closest are %d and %d',
            input_w,
            basenet_stride,
            (input_w - 1) // basenet_stride * basenet_stride + 1,
            ((input_w - 1) // basenet_stride + 1) * basenet_stride + 1,
        )

    if input_h % basenet_stride != 1:
        LOG.warning(
            'input height (%d) should be a multiple of basenet ' 'stride (%d) + 1: closest are %d and %d',
            input_h,
            basenet_stride,
            (input_h - 1) // basenet_stride * basenet_stride + 1,
            ((input_h - 1) // basenet_stride + 1) * basenet_stride + 1,
        )


class Torch2ONNXConverter:
    def __init__(
        self,
        model_input_path: str,
        model_output_path: str,
        input_width: int,
        input_height: int,
        onnx_input_names: List[str],
        onnx_output_names: List[str],
        onnx_opset: int,
        onnx_verbose: bool,
        onnx_export_params: bool,
        onnx_do_constant_folding: bool,
    ):
        self.model_input_path = model_input_path
        self.model_output_path = model_output_path
        self.input_width = input_width
        self.input_height = input_height

        self.params = {}
        self.onnx_input_names = onnx_input_names
        self.onnx_output_names = onnx_output_names
        self.onnx_opset = onnx_opset
        self.onnx_verbose = onnx_verbose
        self.onnx_export_params = onnx_export_params
        self.onnx_do_constant_folding = onnx_do_constant_folding

        self.params = {
            'input_names': self.onnx_input_names,
            'output_names': self.onnx_output_names,
        }
        if self.onnx_opset:
            self.params['opset'] = self.onnx_opset
        if self.onnx_verbose:
            self.params['verbose'] = self.onnx_verbose
        if self.onnx_export_params:
            self.params['export_params'] = self.onnx_export_params
        if self.onnx_do_constant_folding:
            self.params['do_constant_folding'] = self.onnx_do_constant_folding

    def load_model(self):
        model = getattr(models, 'mobilenet')(num_classes=62, widen_factor=1, size=120, mode='small')
        checkpoint_fp = self.model_input_path
        model = load_model(model, checkpoint_fp)
        return model

    def convert(self):
        model = self.load_model()
        inputs = torch.randn(1, 3, self.input_height, self.input_width)

        print(
            "Exporting model to ONNX format at %s... " % self.model_output_path,
            end='',
            flush=True,
        )
        torch.onnx.export(model, inputs, self.model_output_path, do_constant_folding=True, **self.params)
        print('done.')

    def simplify(self):
        for infile in [self.model_output_path]:
            print('Simplifying %s... ' % infile, end='', flush=True)
            simplified_model = onnxsim.simplify(infile, check_n=0, perform_optimization=True, input_shapes={})
            onnx.save(simplified_model, infile.replace('.onnx', '.simp.onnx'))
            print('done.')

    def optimize(self):
        for infile in [self.model_output_path]:
            infile_ = infile.replace('.onnx', '.simp.onnx')

            print('Optimizing %s... ' % infile_, end='', flush=True)
            model = onnx.load(infile_)
            optimized_model = onnx.optimizer.optimize(model)
            onnx.save(optimized_model, infile_.replace('.simp.onnx', '.opt.onnx'))
            print('done')

    def polish(self):
        for infile in [self.model_output_path]:
            infile_ = infile.replace('.onnx', '.opt.onnx')

            print('Polishing %s... ' % infile_, end='', flush=True)
            model = onnx.load(infile_)
            polished_model = onnx.utils.polish_model(model)
            onnx.save(polished_model, infile_.replace('.opt.onnx', '.po.onnx'))
            print('done')

    def check(self):
        for infile in [self.model_output_path]:
            infile_ = infile.replace('.onnx', '.po.onnx')

            print('Checking %s... ' % infile_, end='', flush=True)
            model = onnx.load(infile_)
            onnx.checker.check_model(model)
            print('done')

    def cleanup(self):
        model_dir = os.path.dirname(self.model_output_path)
        print('Cleaning up... ', end='', flush=True)
        for file in os.listdir(model_dir):
            if not file.endswith('.po.onnx'):
                pass
                # os.remove(self.device_dir + '/' + file)
        print('done')


def cli():
    parser = argparse.ArgumentParser(description='Bonseyes AIAsset Framework PyTorch to ONNX exporter tool.')
    parser.add_argument('--model-input', '-mi', type=str, required=True, help='Path to pytorch model')
    parser.add_argument('--model-output', '-mo', type=str, required=True, help='Path to output onnx model')
    parser.add_argument('--input-width', '-iw', type=int, required=True, help='Exported onnx model input width')
    parser.add_argument('--input-height', '-ih', type=int, required=True, help='Exported onnx model input height')
    parser.add_argument('--input-names', '-in', nargs='+', help='ONNX model input names')
    parser.add_argument('--output-names', '-on', nargs='+', help='ONNX model output names')
    parser.add_argument('--opset-version', '-op', type=int, help='ONNX opset version')
    parser.add_argument('--verbose', '-v', dest='verbose', action='store_true', help='ONNX verbose flag')
    parser.add_argument(
        '--export-params', '-ep', dest='export_params', action='store_true', help='ONNX export_params flag'
    )
    parser.add_argument(
        '--do-constant-folding',
        '-cf',
        dest='do_constant_folding',
        action='store_true',
        help='ONNX constant_folding flag',
    )

    return parser


def main():
    args = cli().parse_args()

    converter = Torch2ONNXConverter(
        model_input_path=args.model_input,
        model_output_path=args.model_output,
        input_width=args.input_width,
        input_height=args.input_height,
        onnx_input_names=args.input_names,
        onnx_output_names=args.output_names,
        onnx_opset=args.opset_version,
        onnx_verbose=args.verbose,
        onnx_do_constant_folding=args.do_constant_folding,
        onnx_export_params=args.export_params,
    )
    converter.convert()
    converter.simplify()
    converter.optimize()
    converter.polish()
    converter.check()
    converter.cleanup()


if __name__ == '__main__':
    main()
