import argparse
import os
import sys

sys.path.append("/app/source/Submodule name")

from bonseyes_aiasset.utils.meters import HardwareStatus
from bonseyes_aiasset.export.torch2onnx import Torch2ONNXConverter
try:
    from bonseyes_aiasset.export.onnx2trt import build_tensorrt_engine, GB, MB
except Exception as e:
    pass

ENGINES = ['onnxruntime', 'tensorrt']


def cli():
    parser = argparse.ArgumentParser(description='Bonseyes docker environment runner')
    parser.add_argument(
        '--precisions',
        '-p',
        nargs='+',
        required=True,
        type=str,
        help='Specify target precisions: fp16 fp32'
    )
    parser.add_argument(
        '--input-sizes', '-i', nargs='+', required=True, type=int, help='Specify target input sizes: s1 s2 ...'
    )
    parser.add_argument(
        '--engine',
        '-e',
        required=False,
        default=['all'],
        type=str,
        nargs='+',
        choices=['onnxruntime', 'tensorrt', 'all'],
        help='Choose one or more engines: onnxruntime | tensorrt | all',
    )
    parser.add_argument(
        '--backbone',
        '-bb',
        nargs='?',
        const='mobilenetv1',    # todo change available backbones
        default='mobilenetv1',
        choices=['mobilenetv1', 'mobilenetv0.5', 'resnet22'],
        help='Available backbones: mobilenetv1 | mobilenetv0.5 | resnet22',
    )
    parser.add_argument(
        '--workspace-unit',
        '-wu',
        nargs='?',
        const='GB',
        default='GB',
        choices=['MB', 'GB'],
        help='Available units: MB | GB',
    )
    parser.add_argument(
        '--workspace-size',
        '-ws',
        required=False,
        default=4,
        type=int,
        help='Conversion workspace size in GB',
    )
    parser.add_argument(
        '--enable-dla', '-ed', required=False, default=False, action='store_true', help='Enable dla for tensorrt model'
    )
    parser.add_argument(
        '--onnx-opset-version',
        '-op',
        default=11,
        nargs='?',
        type=int,
        help='ONNX opset version'
    )

    return parser


def main():
    args = cli().parse_args()

    hws = HardwareStatus()
    gpu_name = hws.get()['gpu_name']
    input_sizes = [[ins, ins] for ins in args.input_sizes]
    precisions = args.precisions

    pytorch_models_root = f'/app/bonseyes_aiasset/models/pytorch/{args.backbone}'
    onnx_models_root = f'/app/bonseyes_aiasset/models/onnx/{args.backbone}'
    tensorrt_models_root = f'/app/bonseyes_aiasset/models/tensorrt/{gpu_name}/{args.backbone}'
    os.makedirs(onnx_models_root, exist_ok=True)
    os.makedirs(tensorrt_models_root, exist_ok=True)
    engines = ENGINES if args.engine == ['all'] else args.engine

    files = os.listdir(onnx_models_root)
    for f in files:
        if not f.endswith('.onnx'):
            continue
        os.remove(f'{onnx_models_root}/{f}')

    files = os.listdir(tensorrt_models_root)
    for f in files:
        if not f.endswith('.trt'):
            continue
        os.remove(f'{tensorrt_models_root}/{f}')

    for model in os.listdir(pytorch_models_root):
        if not model.endswith('.pth'):
            continue

        model_path = f'{pytorch_models_root}/{model}'

        for size in input_sizes:
            sw = size[0]
            sh = size[1]
            model_prefix = model.rsplit('_', 2)[0]
            onnx_model_name = f"{model_prefix}_{sw}x{sh}_fp32.onnx"
            model_out_path = f'{onnx_models_root}/{onnx_model_name}'

            if 'onnxruntime' in engines:
                converter = Torch2ONNXConverter(
                    model_input_path=model_path,
                    model_output_path=model_out_path,
                    input_width=sw,
                    input_height=sh,
                    onnx_input_names=None,
                    onnx_output_names=None,
                    onnx_opset=args.onnx_opset_version,
                    onnx_verbose=False,
                    onnx_do_constant_folding=False,
                    onnx_export_params=True,
                )
                converter.convert()

            if 'tensorrt' in engines:
                for precision in precisions:
                    trt_model_name = f"{model_prefix}_{sw}x{sh}_{precision}"

                    dla_enabled = args.enable_dla
                    if dla_enabled:
                        if args.precision == 'fp32':
                            dla_enabled = False
                            trt_model_name += '_dla_disabled'
                        else:
                            trt_model_name += '_dla_enabled'
                    else:
                        trt_model_name += '_dla_disabled'

                    trt_model_name += '.trt'
                    engine_save_path = f'{tensorrt_models_root}/{trt_model_name}'
                    workspace_size = MB(args.workspace_size) if args.workspace_unit == 'MB' else GB(args.workspace_size)

                    build_tensorrt_engine(
                        model_out_path,
                        engine_save_path,
                        precision_mode=precision,
                        max_workspace_size=workspace_size,
                        max_batch_size=1,
                        min_timing_iterations=2,
                        avg_timing_iterations=2,
                        dla_enabled=dla_enabled,
                    )


if __name__ == '__main__':
    main()
