import gdown
import json
import argparse
import os


def download_gz_from_url(url,dest ):
    pass


def download_zip_from_url(url, dest):
    """Download dataset from website url"""
    datasets_name = url.split('/')[-1]
    os.system(f"wget {url} -O {dest}")
    os.system(f'unzip {dest}/{datasets_name}')


def download_gdrive_file(url, dest):
    """Downloading dataset from GDrive registry"""
    gdown.download(url, dest)


def cli():
    parser = argparse.ArgumentParser(description='Bonseyes AI Asset dataset download tool')
    parser.add_argument(
        '--data',
        '-da',
        nargs='?',
        required=True,
        choices=['train', 'eval', 'test', 'all'],
        help='Available dataset splits to download: train | eval | test | all',
    )
    args = parser.parse_args()

    return args


def main():
    args = cli()

    # TODO
    # Implement data downloading

    if args.data == 'train':
        pass
    elif args.data == 'test':
        pass
    elif args.data == 'eval':
        pass
    else:
        pass


if __name__ == '__main__':
    main()
