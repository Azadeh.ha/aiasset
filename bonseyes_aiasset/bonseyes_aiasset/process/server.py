import argparse
import logging
import base64
import sys

from flask import Flask, request, jsonify
from turbojpeg import TurboJPEG

sys.path.append("/app/source/Submodule name")

from bonseyes_aiasset.algorithm.algorithm import Algorithm, AlgorithmInput

log = logging.getLogger('werkzeug')
log.disabled = True

app = Flask(__name__)


def cli():
    parser = argparse.ArgumentParser(description='Bonseyes AI Asset HTTP worker processing. ')
    parser.add_argument('--model', '-mo', required=True, type=str, help='Path to model file')
    parser.add_argument(
        '--input-size',
        '-is',
        required=False,
        default='<default_input_size>',  # TODO add default input size
        type=str,
        help='Model input size in format: WIDTHxHEIGHT',
    )
    parser.add_argument('--port', '-p', required=True, type=int, help='Server port')
    parser.add_argument(
        '--engine',
        '-e',
        nargs='?',
        const='pytorch',
        default='pytorch',
        choices=['pytorch', 'onnxruntime', 'tensorrt'],
        help='Inference engine: pytorch | onnxruntime | tensorrt',
    )
    parser.add_argument(
        '--device',
        '-de',
        nargs='?',
        const='cpu',
        default='cpu',
        choices=['cpu', 'gpu'],
        help='Available devices: cpu | gpu',
    )
    parser.add_argument(
        '--cpu-num',
        '-cn',
        required=False,
        default=None,
        type=int,
        help='Number of CPUs',
    )
    parser.add_argument(
        '--thread-num',
        '-tn',
        required=False,
        default=None,
        type=int,
        help='Number of threads',
    )
    return parser


args = cli().parse_args()

# TODO
# Initialize algorithm
algorithm = Algorithm(
    model_path=args.model,
    engine_type=args.engine,
    input_size=args.input_size,
    device=args.device,
    cpu_num=args.cpu_num,
    thread_num=args.thread_num
    # + additional arguments
)
jpeg_turbo = TurboJPEG()


@app.route('/inference', methods=['POST'])
def perform_inference():
    binary_image = request.get_data()

    width, height, _, _ = jpeg_turbo.decode_header(binary_image)
    image = jpeg_turbo.decode(binary_image)
    algorithm_input = AlgorithmInput(image)
    result = algorithm.process(algorithm_input)

    response = {'prediction': [item.dict for item in result]}

    return jsonify(response)


def main():
    app.run(host='0.0.0.0', port=int(args.port), debug=False)


if __name__ == "__main__":
    main()
