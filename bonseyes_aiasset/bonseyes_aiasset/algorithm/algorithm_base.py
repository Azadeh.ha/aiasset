import numpy as np
import torch
import cv2

from abc import ABC, abstractmethod
from typing import Tuple, Union


class BaseAlgorithmInput(ABC):
    @abstractmethod
    def __init__(self, image: np.array, input_size: Tuple[int, int]) -> None:
        self.image = image
        self.input_size = input_size


class BaseAlgorithmResult(ABC):
    @abstractmethod
    def __init__(self, post_processed_output: np.ndarray) -> None:
        self.post_processed_output = post_processed_output

    @abstractmethod
    def to_object(self) -> object:
        pass

    @abstractmethod
    def to_json(self) -> str:
        pass


class BaseAlgorithm(ABC):
    @abstractmethod
    def __init__(
        self,
        engine: str,
        backbone: str,
        version: str,
        input_size: int,
        mode: str,
        load_from: str = None,
        dla_core: int = None,
    ) -> None:
        self.engine = engine
        self.backbone = backbone
        self.version = version
        self.input_size = input_size
        self.mode = mode
        self.load_from = load_from
        self.dla_core = dla_core

    @abstractmethod
    def load_model(self):
        pass

    @abstractmethod
    def pre_process(
        self, image: Union[np.ndarray, bytes]
    ) -> Union[np.ndarray, torch.FloatTensor, torch.cuda.FloatTensor]:
        pass

    @abstractmethod
    def infer_pytorch(
        self,
        pre_processed_image: Union[
            np.ndarray, torch.FloatTensor, torch.cuda.FloatTensor
        ],
    ) -> Union[np.ndarray, torch.FloatTensor, torch.cuda.FloatTensor]:
        pass

    @abstractmethod
    def infer_onnxruntime(
        self,
        pre_processed_image: Union[
            np.ndarray, torch.FloatTensor, torch.cuda.FloatTensor
        ],
    ) -> Union[np.ndarray, torch.FloatTensor, torch.cuda.FloatTensor]:
        pass

    @abstractmethod
    def infer_tensorrt(
        self,
        pre_processed_image: Union[
            np.ndarray, torch.FloatTensor, torch.cuda.FloatTensor
        ],
    ) -> Union[np.ndarray, torch.FloatTensor, torch.cuda.FloatTensor]:
        pass

    @abstractmethod
    def infer(
        self,
        pre_processed_image: Union[
            np.ndarray, torch.FloatTensor, torch.cuda.FloatTensor
        ],
    ) -> Union[np.ndarray, torch.FloatTensor, torch.cuda.FloatTensor]:
        pass

    @abstractmethod
    def post_process(
        self,
        infer_output: Union[
            np.ndarray, torch.FloatTensor, torch.cuda.FloatTensor
        ],
    ) -> Union[np.ndarray, torch.FloatTensor, torch.cuda.FloatTensor]:
        pass

    @abstractmethod
    def result(
        self,
        post_processed_output: Union[
            np.ndarray, torch.FloatTensor, torch.cuda.FloatTensor
        ],
    ) -> BaseAlgorithmResult:
        pass

    @abstractmethod
    def process(self, input: BaseAlgorithmInput) -> BaseAlgorithmResult:
        pass

    @abstractmethod
    def render(
        self, image: np.ndarray, result: BaseAlgorithmResult
    ) -> np.ndarray:
        pass

