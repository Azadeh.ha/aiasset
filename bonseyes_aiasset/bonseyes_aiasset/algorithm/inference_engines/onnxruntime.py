import onnxruntime as ort
import numpy as np

from .inference_engine import InferenceEngineBase


class OnnxruntimeEngine(InferenceEngineBase):
    """Onnxruntime inference engine wrapper"""

    def __init__(self, model_path, device, thread_num=None, execution_mode='sequential', log_level=3):
        self.device = device
        self.options = ort.SessionOptions()
        self.options.log_severity_level = log_level
        self.options.graph_optimization_level = ort.GraphOptimizationLevel.ORT_ENABLE_ALL
        thread_num = 0 if thread_num is None else thread_num

        if device == 'cpu':
            if execution_mode == 'sequential':
                # Sets the number of threads used to parallelize the execution of the graph
                self.options.intra_op_num_threads = thread_num
                self.options.execution_mode = ort.ExecutionMode.ORT_SEQUENTIAL
            else:
                # Sets the number of threads used to parallelize the execution within nodes
                self.options.inter_op_num_threads = thread_num
                self.options.execution_mode = ort.ExecutionMode.ORT_PARALLEL

        self.session = self.load(model_path)
        self.input_names = [inp.name for inp in self.session.get_inputs()]
        self.output_names = [otp.name for otp in self.session.get_outputs()]

    def destroy(self):
        pass

    def load(self, model_path):
        if self.device == 'cpu':
            session = ort.InferenceSession(model_path, sess_options=self.options, providers=['CPUExecutionProvider'])
        else:
            session = ort.InferenceSession(model_path, None, providers=['CUDAExecutionProvider'])

        return session

    def infer(self, image: np.ndarray, input: dict = None):
        if input is None:
            input = {self.input_names[0]: image}
            predictions = self.session.run(None, input)
        else:
            predictions = self.session.run(None, input)

        return predictions
