import numpy as np
import torch
import cv2

from typing import Tuple, Union

from algorithm_base import BaseAlgorithm, BaseAlgorithmInput, BaseAlgorithmResult


class AlgorithmInput(BaseAlgorithmInput):
    pass


class AlgorithmResult(BaseAlgorithmResult):
    pass


class Algorithm(BaseAlgorithm):
    pass

