#define CATCH_CONFIG_MAIN

#include <iostream>

#include "../deps/catch2/catch.hpp"
#include "../deps/jsonl/json.hpp"


using namespace std;


SCENARIO("<AiAssetName> end-to-end test on <AiAssetDatasetName> eval subset", "" ) {
    GIVEN("ground truths for each eval image") {

        WHEN("end-to-end algorithm execution is performed using onnxruntime inference engine") {
            THEN("produced results match ground truths") {
                REQUIRE(true == true);
            }
        }

        WHEN("end-to-end algorithm execution is performed using tensorrt inference engine") {
            THEN("produced results match ground truths") {
                REQUIRE(true == true);
            }
        }
    }
}