#pragma once

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <vector>


class Renderer {
public:
    Renderer();
    cv::Mat render(const cv::Mat &image);
};

