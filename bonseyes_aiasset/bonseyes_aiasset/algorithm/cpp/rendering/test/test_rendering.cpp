#define CATCH_CONFIG_MAIN

#include <iostream>

#include "../../deps/catch2/catch.hpp"
#include "../../deps/jsonl/json.hpp"

#include "../include/renderer.hpp"


using namespace std;


SCENARIO("<AiAssetName> rendering test", "" ) {
    GIVEN("Input image and network predictions") {

        WHEN("rendering is applied on the image") {
            THEN("produced results render predictions properly") {
                REQUIRE(true == true);
            }
        }
    }
}