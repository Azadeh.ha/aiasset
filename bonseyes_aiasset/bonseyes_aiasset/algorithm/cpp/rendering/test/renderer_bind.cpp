#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <vector>

#include "../../deps/ndarray_converter/ndarray_converter.hpp"

using namespace std;


cv::Mat render(cv::Mat image) {
    return image;
}

PYBIND11_PLUGIN(openpifpaf_renderer_bind) {
    NDArrayConverter::init_numpy();

    pybind11::module m("renderer_bind", "<AiAssetName> renderer bind");
    m.def("render", &render, "<AiAssetName> rendering main method", pybind11::arg("image"));

    return m.ptr();
}
