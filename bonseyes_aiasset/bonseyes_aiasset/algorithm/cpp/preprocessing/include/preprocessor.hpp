#pragma once

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>

#include <memory>
#include <vector>


class AlgorithmPreprocessor {
    public:
        AlgorithmPreprocessor();
        cv::Mat process(const cv::Mat& image);
};