#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <vector>

#include "../../deps/ndarray_converter/ndarray_converter.hpp"
#include "../include/preprocessor.hpp"

using namespace std;


cv::Mat preprocess(cv::Mat image) {
    return image;
}

PYBIND11_PLUGIN(openpifpaf_preprocessor_bind) {
    NDArrayConverter::init_numpy();

    pybind11::module m("preprocessor_bind", "<AiAssetName> preprocessing bind");
    m.def("preprocess", &preprocess, "<AiAssetName> preprcoessor main method", pybind11::arg("image"));

    return m.ptr();
}
