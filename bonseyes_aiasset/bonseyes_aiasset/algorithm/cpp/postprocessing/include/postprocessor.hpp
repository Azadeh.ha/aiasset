#pragma once

#include "data_structures/blobnd.hpp"


class AlgorithmPostprocessor {
    public:
        AlgorithmPostprocessor();
        void process();
        std::string json_predictions();
};