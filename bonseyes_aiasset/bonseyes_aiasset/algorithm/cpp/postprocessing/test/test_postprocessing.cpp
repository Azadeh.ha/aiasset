#define CATCH_CONFIG_MAIN

#include <streambuf>
#include <iostream>
#include <fstream>
#include <vector>

#include <dirent.h>

#include "../include/aiasset_postprocessor.hpp"
#include "../../deps/catch2/catch.hpp"
#include "../../deps/jsonl/json.hpp"


using namespace std;
using namespace nlohmann;


SCENARIO("<AiAssetName> postprocessing test on <AiAssetDatasetName> eval subset", "" ) {
    GIVEN("network outputs and ground truth data in json files") {
        string ground_truth_data_root = "";

        WHEN("postprocessing is applied on network outputs") {
            THEN("produced results match ground truths") {
                REQUIRE(true == true);
            }
        }
    }
}