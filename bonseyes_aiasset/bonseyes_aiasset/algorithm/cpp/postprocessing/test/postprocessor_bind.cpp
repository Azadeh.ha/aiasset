#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include <vector>

#include "../include/aiasset_postprocessor.hpp"

using namespace std;
namespace py = pybind11;


PYBIND11_PLUGIN(aiasset_postprocessor_bind) {
    py::module m("aiasset_postprocessor_bind", "aiasset postprocessing bind");

    py::class_<AlgorithmPostprocessor>(m, "AlgorithmPostprocessor")
        .def(py::init<>())
        .def("json_predictions", &AlgorithmPostprocessor::json_predictions);

    return m.ptr();
}
