import platform

# 1. filepath
# 2. resize pipe
# 3. rotate pipe
default_capture_pipeline = ''' 
   filesrc location=%s ! decodebin name=dec ! queue  ! videoconvert ! queue !
   video/x-raw, format=BGR !
   %s
   %s
   appsink 
'''

default_sink_pipeline = '''
    appsrc ! videoconvert ! video/x-raw,format=I420 ! x264enc ! mp4mux ! filesink location=%s
'''

nvidia_capture_pipeline = '''
   filesrc location=%s ! 
   qtdemux ! queue ! h264parse ! omxh264dec ! nvvidconv ! video/x-raw,format=BGRx !  
   queue ! videoconvert ! queue ! video/x-raw, format=BGR ! 
   %s
   %s
   appsink
   
'''
nvidia_sink_pipeline = '''
   appsrc ! video/x-raw, format=(string)BGR ! videoconvert !
   video/x-raw, format=(string)I420 ! nvvidconv !
   video/x-raw(memory:NVMM) ! nvv4l2h264enc ! h264parse !
   matroskamux !
   filesink location=%s 
'''

# for x86_64, x86_32 and rpi choose default pipeline
capture_pipeline = default_capture_pipeline
sink_pipeline = default_sink_pipeline

if platform.machine() == 'aarch64' and 'raspi' not in platform.platform():
    capture_pipeline = nvidia_capture_pipeline
    sink_pipeline = nvidia_sink_pipeline