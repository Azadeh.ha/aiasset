import platform
import psutil
import sys

if platform.machine() == 'x86_64':
    try:
        import nvidia_smi
    except Exception as e:
        pass

if platform.machine() == 'aarch64':
    # jetson
    if platform.version().split('-')[-1] == 'tegra':
        pass
    # raspberry
    elif platform.version().split('-')[-1] == 'raspi':
        pass


class HardwareStatus:
    def __init__(self):
        self.smi_handle = None
        self.memory_used_on_start = 0

        if platform.machine() == 'x86_64':
            try:
                nvidia_smi.nvmlInit()
                self.smi_handle = nvidia_smi.nvmlDeviceGetHandleByIndex(0)
                info = nvidia_smi.nvmlDeviceGetMemoryInfo(self.smi_handle)
                self.memory_used_on_start = info.used / 1e9
            except Exception as e:
                pass

        if platform.machine() == 'aarch64':
            pass

    def get(self):
        gpu_name = 'none'
        gpu_usage = 0
        gpu_mem_usage = 0
        gpu_mem_used = 0
        cpu_usage = psutil.cpu_percent()

        if platform.machine() == 'x86_64':
            try:
                gpu_name = nvidia_smi.nvmlDeviceGetName(self.smi_handle).decode()
                gpu_name = gpu_name.replace(' ', '_')
                gpu_usage = nvidia_smi.nvmlDeviceGetUtilizationRates(self.smi_handle).gpu
                gpu_mem_usage = nvidia_smi.nvmlDeviceGetUtilizationRates(self.smi_handle).memory
                info = nvidia_smi.nvmlDeviceGetMemoryInfo(self.smi_handle)
                gpu_mem_used = info.used / 1e9
            except Exception as e:
                pass

        if platform.machine() == 'aarch64':
            gpu_name = 'aarch64'
            gpu_usage = 0
            gpu_mem_used = 0
            gpu_mem_usage = gpu_mem_used

        return {
            'cpu_usage': cpu_usage,
            'gpu_name': gpu_name,
            'gpu_usage': gpu_usage,
            'gpu_mem_usage': gpu_mem_usage,
            'gpu_mem_used': gpu_mem_used,
        }


if __name__ == '__main__':
    hs = HardwareStatus()
    print(hs.get())
