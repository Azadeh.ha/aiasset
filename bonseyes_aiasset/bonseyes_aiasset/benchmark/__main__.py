import argparse
import datetime
import time
import cv2
import os
import re
import sys
import json
import psutil
import numpy as np
from tqdm import tqdm

from bonseyes_aiasset.algorithm.algorithm import Algorithm, AlgorithmInput
from bonseyes_aiasset.utils.meters import HardwareStatus

TEST_DATASETS = []  # List of test datasets


def benchmark(
    dataset_path,
    algorithm,
    engine,
    input_size,
    device,
    number_cpus,
    number_threads,
    warm_up_num
):
    """
    Benchmark function that will evaluate model and store the
    """
    # TODO
    # Implement or reuse benchmark for AI Asset

    result = {
        'NAME': None,
        'DATASET': None,
        'BACKBONE': None,
        'INPUT_SIZE': None,
        'ENGINE': None,
        'PRECISION': None,
        'NME': None,
        'STD': None,
        'STORAGE': None,
        'LATENCY': None,
        'PREPROCESSING TIME': None,
        'INFERENCE TIME': None,
        'POSTPROCESSING TIME': None,
        'DEVICE': None,
        'CPU_NUM': None,
        'THREAD_NUM': None,
        'GPU_MEMORY': None,
        'EVAL_TIME': None,
    }

    return result


def cli():
    parser = argparse.ArgumentParser(description='Bonseyes AI Asset evaluation.')
    parser.add_argument('--model', '-mo', required=True, type=str, help='Path to model file')
    parser.add_argument('--input-size', '-is', required=True, type=int, nargs='+', help='Model input size')
    parser.add_argument(
        '--dataset',
        '-da',
        nargs='?',
        required=True,
        type=str,
        choices=TEST_DATASETS + ['all'],
        help=f"Available devices: {TEST_DATASETS + ['all']}",
    )
    parser.add_argument(
        '--engine',
        '-e',
        required=True,
        default='pytorch',
        type=str,
        help='Inference engine: pytorch | onnxruntime | tensorrt',
    )
    parser.add_argument(
        '--device',
        '-de',
        nargs='?',
        const='cpu',
        default='cpu',
        choices=['cpu', 'gpu'],
        help='Available devices: cpu | gpu',
    )
    parser.add_argument(
        '--warm-up-num',
        '-wn',
        default=100,
        required=False,
        type=int,
        help='Number of images for a warm up.',
    )
    parser.add_argument(
        '--cpu-num',
        '-cn',
        required=False,
        default=None,
        type=int,
        help='Number of CPUs',
    )
    parser.add_argument(
        '--thread-num',
        '-tn',
        required=False,
        default=None,
        type=int,
        help='Number of threads',
    )

    return parser


def main():
    args = cli().parse_args()

    algorithm = Algorithm(
        model_path=args.model,
        engine_type=args.engine,
        input_size=args.input_size,
        device=args.device,
        cpu_num=args.cpu_num,
        thread_num=args.thread_num,
    )

    number_threads = '-'
    if args.thread_num is not None:
        os.environ["OMP_NUM_THREADS"] = f'{args.thread_num}'
        if args.device == 'cpu':
            number_threads = args.thread_num

    number_cpus = '-'
    if args.cpu_num:
        p = psutil.Process()
        if psutil.cpu_count() < args.cpu_num:
            raise Exception(
                f"Specified number of CPUs exceeds available number. "
                f"Available CPUs on machine: {psutil.cpu_count()}"
            )
        cpu_list = [*range(0, args.cpu_num)]
        p.cpu_affinity(cpu_list)
        if args.device == 'cpu':
            number_cpus = args.cpu_num

    datasets = TEST_DATASETS if args.dataset == 'all' else [args.dataset]
    for dataset in datasets:
        # TODO
        # Add proper benchmark arguments
        result = benchmark()
        algorithm.destroy()
        print(f"{args.dataset.upper()} results: ")
        print(result)

        result_dir = '/app/eval_results'
        if not os.path.exists(result_dir):
            os.makedirs(result_dir)

        with open(f'{result_dir}/result_{dataset}.json', 'w') as json_result:
            json.dump(result, json_result, indent=2)


if __name__ == '__main__':
    main()