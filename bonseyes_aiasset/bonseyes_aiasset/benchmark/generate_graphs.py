import matplotlib.pyplot as plt
import csv
import random
import argparse

from typing import Union


def extract_data(csv_path, metric)->dict:
    with open(csv_path, 'r') as csvfile:
        plots = csv.reader(csvfile, delimiter=',')
        # isolate header row
        for row in plots:
            header_row = row
            break
        # get indices of necessary fields
        index = {'BACKBONE': 0, 'INPUT_SIZE': 0, 'ENGINE': 0, 'PRECISION': 0, metric: 0}
        num = 0
        for item in header_row:
            if item in index.keys():
                index[item] = num
            num += 1

        data = dict()

        for row in plots:
            backbone = row[index['BACKBONE']]
            input_size = row[index['INPUT_SIZE']]
            engine = row[index['ENGINE']]
            precision = row[index['PRECISION']]
            curr_metric = row[index[metric]]

            engine_precision = (engine, precision)
            if backbone not in data:
                data[backbone] = dict()
            if engine_precision not in data[backbone]:
                data[backbone][engine_precision] = dict()

            # eg data['resnet50'][('pytorch', 'fp32')]['128x128']
            data[backbone][engine_precision][input_size] = float(curr_metric)
        return data


def plot_metrics(
        csv_path: str,
        metrics: list,
        output_path='/app'
):
    num_plots = len(metrics)
    # fig, axs = plt.subplots(num_plots, figsize=(10,45))
    figures = dict()
    # fig.suptitle('Comparison of metrics between different engines and precisions')
    color = dict()
    style=dict()
    styles = ['--', '-.', '-']
    colors = ["#E54421", "#125FBB", "#E39017", "#2D960B", "#B049DC"]
    for idx, metric in enumerate(metrics):
        data = extract_data(metric=metric, csv_path=csv_path)
        for backbone in data.keys():
            if idx == 0:
                figures[backbone] = plt.subplots(num_plots, figsize=(10,45), constrained_layout=True)
                figures[backbone][0].suptitle(backbone, fontsize=12)

            bb_data = data[backbone]
            # extract input sizes for 1st (engine, precision) pair
            info = next(iter(bb_data.values()))
            x = list(info.keys())

            # sort input sizes in increasing order
            x_numeric = [int(size.split('x')[0]) * int(size.split('x')[1]) for size in x]
            x = [input_size for _, input_size in sorted(zip(x_numeric, x))]
            engine_num = 0
            for (engine, precision), info in bb_data.items():
                # choose color for each (engine, precision) pair
                if metric == metrics[0]:
                    if (engine, precision) not in color:
                        color[(engine, precision)] = colors[engine_num % len(colors)]
                    if (engine, precision) not in style:
                        style[(engine, precision)] = styles[ engine_num % len(styles)]
                        engine_num += 1

                # add plot
                y = []
                for num in range(len(x)):
                    y.append(info[x[num]])
                figures[backbone][1][idx].plot(x, y, color=color[(engine, precision)], marker='o', label = f'{engine}, {precision}',
                              linestyle=style[(engine, precision)])
                if metric == 'STORAGE':
                    unit = '[MB]'
                elif metric in ['LATENCY', 'PREPROCESSING TIME', 'INFERENCE TIME', 'POSTPROCESSING TIME']:
                    unit = '[ms]'
                else:
                    unit = ''

                ylabel = metric + ' ' + unit
                figures[backbone][1][idx].set_title(f'{metric}')
                figures[backbone][1][idx].set_xlabel('INPUT SIZE')
                figures[backbone][1][idx].set_ylabel(ylabel)
                figures[backbone][1][idx].legend()


    for backbone in data.keys():
        #figures[backbone][0].tight_layout()
        figures[backbone][0].savefig(f'{output_path}/graph_{backbone}.jpg', dpi=300)
    print('Metric graph generated successfully. Saved in', output_path)


def cli():
    parser = argparse.ArgumentParser(description='Generate metric graphs from csv file with eval results')
    parser.add_argument('--csv-path', '-cp', required=True, type=str,
        help='Path to csv file that contains evaluation results'
    )
    parser.add_argument('--output-path', '-op', required=True, type=str,
        help='Path to store output graph.jpg file'
    )
    args = parser.parse_args()

    return args


def main():
    args = cli()
    plot_metrics(
        metrics=['STORAGE', 'LATENCY', '<PRECISION_METRICS>', 'PREPROCESSING TIME', 'INFERENCE TIME', 'POSTPROCESSING TIME'],
        csv_path=args.csv_path,
        output_path=args.output_path)


if __name__ == '__main__':
    main()