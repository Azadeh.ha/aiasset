import argparse
import os


def cli():
    parser = argparse.ArgumentParser(description='Bonseyes AI Asset docker images runner.')
    parser.add_argument('--container-name', '-cnm', required=True, type=str, help='Specify container name')
    parser.add_argument('--image-name', '-inm', required=True, type=str, help='Specify built image name')
    parser.add_argument('--port', '-p', type=str, help='Specify container port to expose for http worker')
    args = parser.parse_args()

    return args


def main():
    args = cli()

    container_name = args.container_name
    image_name = args.image_name
    port = args.port

    os.system(
        f'''
        docker run --name {container_name} --rm -it \
            --gpus=all \
            --ipc=host \
            -v $(pwd):/app \
            -v /tmp/.X11-unix:/tmp/.X11-unix \
            --device /dev/video0 \
            -e DISPLAY=$DISPLAY \
            -p {port}:{port} \
            {image_name}
    '''
    )


if __name__ == '__main__':
    main()
